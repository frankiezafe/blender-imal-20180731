# Introduction à la Modélisation 3D

notes et fichiers créés durant le cours de 3d dans [blender](http://blender.org) à imal

* page officielle: http://imal.org/fr/summerworkshops2018/3d
* wiki: http://wiki.imal.org/workshop/introduction-%C3%A0-la-mod%C3%A9lisation-3d-blender-31-juillet-3-ao%C3%BBt

## dump du pad

adresse initiale, avec historique: https://etherpad.artsaucarre.be/p/imal-summer2018

### blender.org

* http://wiki.imal.org/fuzzysearch/results/blender
* https://pythonapi.upbge.org/contents.html
* https://blendermarket.com/products/retopoflow/ - https://github.com/CGCookie/retopoflow


### puredata
* http://puredata.info/
* example avec karlax https://github.com/frankiezafe/blender-pd
* resources:
* * https://github.com/Blender-Brussels
* * https://blender-brussels.github.io/articles/Kinect-Melon-OSC-BGE/
* * http://nikitron.cc.ua/sverchok_en.html / https://github.com/nortikin/sverchok/
* * http://wiki.imal.org/workshop/introduction-%C3%A0-la-mod%C3%A9lisation-3d-blender-31-juillet-3-ao%C3%BBt
* * http://f-lat.org/kiwi/doku.php/dev:karlax

* Sverchok: https://github.com/Blender-Brussels/bpy-bge-library/tree/master/users/frankiezafe/bouctje

### dessin vectoriel
* https://inkscape.org/en/ (cross-platform, libre)

svg telechargeables: 
* https://upload.wikimedia.org/wikipedia/commons/6/67/Firefox_Logo%2C_2017.svg
* https://svgsilh.com/2196f3/image/2255910.html

### vérifier l'export pour impression
* http://slic3r.org/
* https://ultimaker.com/en/resources/manuals/software

### sources de modèles
* https://www.thingiverse.com/
* https://sketchfab.com/
* https://www.archive3d.net/
* https://www.blendswap.com/

### python
*  https://gitlab.com/frankiezafe/Blender-scripts

### social networks
* http://frankiezafe.org
* https://mastodon.xyz/@frankiezafe
* https://twitter.com/frankiezafe

vers substance painter
* https://blender.stackexchange.com/questions/28398/is-there-a-way-to-create-a-color-id-map-in-blender

//////////////////////////////////////////
participants::
peter.friess@gmx.com
https://www.facebook.com/PETERMFRIESS
YIAP -  https://www.facebook.com/yiapbrussels
mardaga88@gmail.com
https://www.facebook.com/milva.mardaga
participant
jjdr
http://pourvuquonaitlivress.blog.lemonde.fr/
https://www.facebook.com/JJDeRette
freddgeorge@yahoo.com
https://www.facebook.com/freddgeorge

/////////////////////////////////////////

** attention: les indentations nécessaires au bon fonctionnement du python peuvent avoir été mal menées à cause du copier/coller **


`
import bpy
cube = bpy.data.objects["Cube"]
for v in cube.data.vertices:
    print( v )
print( cube.data )
`

`
import bpy
cube = bpy.data.objects["Cube"]
for v in cube.data.vertices:
    print( v.co )
print( cube.data )
`

`
import bpy
cube = bpy.data.objects["Cube"]
for v in cube.data.vertices:
    print( v.co )
    v.co[0] *= 1.3
    v.co[1] *= 0.5
print( cube.data )
`

`
import bpy
import math
import mathutils
cube = bpy.data.objects["Cube"]
for v in cube.data.vertices:
    print( v.co )
    v.co[0] *= mathutils.noise.random()
    v.co[1] *= mathutils.noise.random()
    v.co[2] *= mathutils.noise.random()
print( cube.data )
`

`
import bpy
import math
import mathutils
cube = bpy.data.objects["Cube"]
cube.rotation_euler.x += 0.1
cube.rotation_euler.y += 0.1
cube.rotation_euler.z += 0.1
`

`
import bpy
import math
import mathutils
bpy.ops.mesh.primitive_cube_add(location=(0, 0, 0))
`

`
import bpy
import math
import mathutils
for x in range( 0,10 ):
    bpy.ops.mesh.primitive_cube_add(location=(x, 0, 0))
`

`
import bpy
import math
import mathutils
for x in range( 0,10 ):
    for y in range( 0,10 ):
        bpy.ops.mesh.primitive_cube_add(location=(x * 2.5, y * 2.5, 0))
`

`
import bpy
import math
import mathutils
print( "**************************************" )
scy = 0.1
for x in range( 0,10 ):
    for y in range( 0,10 ):
        bpy.ops.mesh.primitive_cube_add(location=(x * 2.5, y * 2.5, 0))
        cube_courant = bpy.context.scene.objects.active
        cube_courant.scale[2] = scy
        scy += 0.1
        #print( bpy.context.scene.objects.active.name )
`

`
import bpy
import math
import mathutils
size = 3
min = 0.5
mult = 0.1
for x in range( -size,size+1 ):
    for y in range(-size,size+1 ):
        bpy.ops.mesh.primitive_cube_add(location=(x * 2.5, y * 2.5, 0))
        cube_courant = bpy.context.scene.objects.active
        cube_courant.scale[2] = min + abs( x *y ) * mult
`

https://blenderartists.org/t/blender-2-6-set-keyframes-using-python-script/525513/2

`
import bpy
import math
import mathutils
size = 3
min = 0.5
mult = 0.1
for x in range( -size,size+1 ):
    for y in range(-size,size+1 ):
        bpy.ops.mesh.primitive_cube_add(location=(x * 2.5, y * 2.5, 0))
        cube_courant = bpy.context.scene.objects.active
        cube_courant.scale[2] = min + abs( x *y ) * mult
        bpy.context.scene.frame_current = 1
        bpy.ops.anim.keyframe_insert_menu(type='Location')
        bpy.context.scene.frame_current = 100
        cube_courant.location[2] += y
        bpy.ops.anim.keyframe_insert_menu(type='Location')
`

`
import bpy
import math
import mathutils
size = 3
min = 0.5
mult = 0.8
for x in range( -size,size+1 ):
    for y in range(-size,size+1 ):
        bpy.ops.mesh.primitive_cube_add(location=(x * 2.5, y * 2.5, 0))
        cube_courant = bpy.context.scene.objects.active
        cube_courant.scale[2] = min + abs( x *y ) * mult
        bpy.context.scene.frame_current = 1
        bpy.ops.anim.keyframe_insert_menu(type='Location')
        bpy.context.scene.frame_current = 100
        cube_courant.location[2] += y * x
        bpy.ops.anim.keyframe_insert_menu(type='Location')
        bpy.context.scene.frame_current = 200
        cube_courant.location[2] = 0
        bpy.ops.anim.keyframe_insert_menu(type='Location')
bpy.ops.render.render(animation=True)
quit()
`

//////////////////////////////////////////////////////////////

`
import bge
import math
scene = bge.logic.getCurrentScene()
root = scene.objects["root"]
if 'frame_count' not in root:
    root['frame_count'] = 0
    root['cubes'] = []
    for o in scene.objects:
        if o.name[0:4] == 'Cube':
            print( o.name )
            o['init_x'] = o.position[0]
            o['init_y'] = o.position[1]
            o['init_z'] = o.position[2]
            root['cubes'].append(o)
else:
    root['frame_count'] += 1
    
for o in root['cubes']:
    a = ( root['frame_count'] * 0.03 + o['init_x'] ) 
    o.position[0] = math.cos( a ) * 5
    o.position[2] = math.sin( a )
#print( root['frame_count'] )
`

/////////////////////////// PD PATCH

#N canvas 87 94 744 625 10;
#X obj 150 406 dac~;
#X obj 157 263 osc~ 440;
#X obj 162 344 *~;
#X obj 235 170 vsl 15 128 0 1 0 0 empty empty empty 0 -9 0 10 -262144
-1 -1 0 1;
#X obj 115 299 *~;
#X obj 57 233 osc~ 480;
#X obj 40 77 vsl 15 128 0 800 0 0 empty empty empty 0 -9 0 10 -262144
-1 -1 300 1;
#X obj 400 290 packOSC;
#X obj 448 44 vsl 15 128 0 127 0 0 empty empty empty 0 -9 0 10 -262144
-1 -1 7800 1;
#X msg 450 199 send /test \$1;
#X obj 400 374 udpsend;
#X msg 495 297 connect 127.0.0.1 23000;
#X obj 568 44 vsl 15 128 0 127 0 0 empty empty empty 0 -9 0 10 -262144
-1 -1 12000 1;
#X obj 329 128 metro 100;
#X obj 329 150 f;
#X obj 329 172 + 1;
#X msg 570 199 send /test2 \$1;
#X msg 329 197 send /count \$1;
#X msg 495 328 disconnect;
#X obj 329 96 tgl 15 0 empty empty empty 17 7 0 10 -262144 -1 -1 0
1;
#X connect 1 0 4 1;
#X connect 2 0 0 0;
#X connect 2 0 0 1;
#X connect 3 0 2 1;
#X connect 4 0 2 0;
#X connect 5 0 4 0;
#X connect 6 0 5 0;
#X connect 7 0 10 0;
#X connect 8 0 9 0;
#X connect 9 0 7 0;
#X connect 11 0 10 0;
#X connect 12 0 16 0;
#X connect 13 0 14 0;
#X connect 14 0 15 0;
#X connect 15 0 14 1;
#X connect 15 0 17 0;
#X connect 16 0 7 0;
#X connect 17 0 7 0;
#X connect 18 0 10 0;
#X connect 19 0 13 0;

//////////////////////////////////////////


`
import bpy
import math
import mathutils
penta = bpy.data.objects['penta_guide']
def create_montant( pos, name, index ):
    
    # cutter (pour le boolean)
    bpy.ops.mesh.primitive_cube_add( location = pti.to_tuple() )
    bpy.context.object.draw_type = 'WIRE'
    c1 = bpy.context.scene.objects.active
    c1.name = name + "_cuter_" + str(index)
    for v in c1.data.vertices:
        if v.co[0] < 0:
            v.co[0] = -0.3
        else:
            v.co[0] *= 1.6
        v.co[1] *= 0.03
        v.co[2] *= 0.1
    
    # objet à couper
    bpy.ops.mesh.primitive_cube_add( location = pti.to_tuple() )
    c0 = bpy.context.scene.objects.active
    c0.name = name + str(index)
    for v in c0.data.vertices:
        if v.co[0] < 0:
            v.co[0] = -0.3
        else:
            v.co[0] *= 1.6
        v.co[1] *= 0.03
        v.co[2] *= 0.03
    
    return c0, c1
for i in range( 0, len( penta.data.vertices ) ):
    j = i + 1
    if j >= len( penta.data.vertices ):
        j = 0
    pti = mathutils.Vector( penta.data.vertices[i].co )
    ptj = mathutils.Vector( penta.data.vertices[j].co )
    
    latte, cutter = create_montant( pti.to_tuple(), 'penta', i )
    
    front = ( ptj - pti ).normalized()
    left = front.cross( mathutils.Vector((0,0,1)) )
    up = left.cross( front )
    
    m = mathutils.Matrix().to_3x3()
    m[0][0], m[1][0], m[2][0] = front.to_tuple()
    m[0][1], m[1][1], m[2][1] = left.to_tuple()
    m[0][2], m[1][2], m[2][2] = up.to_tuple()
    
    latte.rotation_euler = m.to_euler()
    cutter.rotation_euler = m.to_euler()
    
    #c.rotation_euler[2] = math.atan2( normal.y, normal.x )
    #print( dir, dir.length )
`
    
    
`
import bge
import math
    
############################# OSC BEGIN
import socket
import bge.logic as gl
import socket
from OSC.OSC import decodeOSC, OSCClient, OSCMessage
n = 0
buffer_size = 1024
config = {   0: ('127.0.0.1', 23000, buffer_size ) }
scene = gl.getCurrentScene()
cont = gl.getCurrentController()
obj = cont.owner
# SETUP
if not 'OSC_LOADED' in obj:
    obj["OSC_LOADED"] = 1
    obj['OSC_CLIENT'] = OSCClient()
    obj['OSC_CLIENT'].connect( ("127.0.0.1", 7110) )
    # ------------ Set list for get_osc ---------------- #
    gl.connected = []
    gl.socket = []
    for i in range( len( config ) ):
        gl.connected.append(False)
        gl.socket.append(0)
    # ------------ Set some var ---------------- #
    gl.tempo = 0
    ip = config[n][0]
    port = config[n][1]
    buffer_size = config[n][2]
    # Connect Blender
    if not gl.connected[n] :
        gl.socket[n] = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            gl.socket[n].bind((ip, port))
            gl.socket[n].setblocking(0)
            gl.socket[n].settimeout(0.01)
            gl.socket[n].setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, buffer_size)
            print('Plug : IP = {} Port = {} Buffer Size = {} Instance = {}'.format(ip, port, buffer_size, n))
            gl.connected[n] = True
        except:
            print('No connected to IP = {} Port = {}'.format(ip, port))
            pass
# If Blender connected
if gl.connected[0] :
    try:
        raw_data = gl.socket[n].recv( buffer_size )
        data = decodeOSC(raw_data)
        print( data )
        if data[0] == '/speed' and 'speed' in obj:
            obj['speed'] = data[2]
    except socket.error:
        pass
   
############################# OSC END
if 'frame_count' not in obj:
    obj['speed'] = 0.03
    obj['frame_count'] = 0
    obj['cubes'] = []
    for o in scene.objects:
        if o.name[0:4] == 'Cube':
            print( o.name )
            o['init_x'] = o.position[0]
            o['init_y'] = o.position[1]
            o['init_z'] = o.position[2]
            obj['cubes'].append(o)
else:
    obj['frame_count'] += 1
    
for o in obj['cubes']:
    a0 = ( obj['frame_count'] * obj['speed'] + o['init_x'] ) 
    a1 = ( obj['frame_count'] * obj['speed'] + o['init_y'] ) 
    o.position[2] = math.sin( a0 ) * math.sin( a1 )
msg = OSCMessage()
msg.setAddress( "/frame_count" )
msg.append( obj['frame_count'] )
obj['OSC_CLIENT'].send( msg )
`

https://github.com/ptone/pyosc/blob/master/examples/knect-snd.py
